ssl_session_cache   shared:ssl_session_cache:10m;

server {
    listen      80;
    server_name renovafacil.gabrielsp.com;

    return 301  https://$host$request_uri;
}

server {
    listen                  443 ssl;
    server_name             renovafacil.gabrielsp.com;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_certificate         /etc/letsencrypt/live/renovafacil.gabrielsp.com/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/renovafacil.gabrielsp.com/privkey.pem;

    add_header  Strict-Transport-Security   max-age=31536000;

    location / {
        root   /var/www;
        index  index.html;
        try_files $uri $uri/ @rewrites;
    }

    location /.well-known {
        root /var/www/.well-known;
    }

    location @rewrites {
        rewrite ^(.+)$ /index.html last;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location /api/ {
        proxy_pass  http://backend:3000;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}

